package com.nCinga;

public class Transaction extends Thread{
    Account account;
    TransactionType transactionType;
    double transactionAmount;

    public Transaction(Account account, TransactionType transactionType, double amount){
        this.account=account;
        this.transactionType=transactionType;
        this.transactionAmount=amount;
    }

    synchronized void deposit(double amount){
        account.deposit(amount);
        System.out.println("Deposit Success: "+transactionAmount+"LKR");
    }

    synchronized void withdraw(double amount){
        if(account.withdraw(amount))
            System.out.println("Withdraw Success: "+transactionAmount+"LKR");
        else
            System.out.println("Withdraw Failed: Insufficient Balance");
    }

    @Override
    public void run() {
        if(transactionType==TransactionType.DEPOSIT){
            deposit(transactionAmount);
        }
        else if(transactionType==TransactionType.WITHDRAWAL){
            withdraw(transactionAmount);
        }
    }
}



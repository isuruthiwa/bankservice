package com.nCinga;

public class Account {
    int accountNo;
    private static int accountNoCounter=0;
    String name;
    double balance;

    public Account(String name, int deposit){
        this.accountNo=++accountNoCounter;
        this.name=name;
        this.balance=deposit;
    }

    public String getName() {
        return name;
    }

    private void setName(String name) {
        this.name = name;
    }

    public double getBalance() {
        return balance;
    }

    private void setBalance(double balance) {
        this.balance = balance;
    }

    public boolean withdraw(double amount){
        if(this.balance>=amount){
            this.balance-=amount;
            return true;
        }
        else
            return false;
    }

    public void deposit(double amount){
        this.balance+=amount;
    }
}
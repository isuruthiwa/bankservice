package com.nCinga;

public class Bank {
    public static void main(String[] args) {
        Account acc1=new Account("Kamal",1000);

        Transaction transaction1=new Transaction(acc1,TransactionType.DEPOSIT,1000);
        Transaction transaction2=new Transaction(acc1,TransactionType.DEPOSIT,2000);
        Transaction transaction3=new Transaction(acc1,TransactionType.DEPOSIT,3000);
        Transaction transaction4=new Transaction(acc1,TransactionType.DEPOSIT,1000);
        Transaction transaction5=new Transaction(acc1,TransactionType.DEPOSIT,2000);
        Transaction transaction6=new Transaction(acc1,TransactionType.WITHDRAWAL,3000);
        transaction1.start();
        transaction2.start();
        transaction3.start();
        transaction4.start();
        transaction5.start();
        transaction6.start();

        try {
            Thread.sleep(100);
        } catch (InterruptedException inter) {
            inter.printStackTrace();
        }


        System.out.println("Account Name : "+acc1.getName());
        System.out.println("Account Balance : "+acc1.getBalance());

    }
}
